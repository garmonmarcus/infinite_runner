//
// Created by Marcus Garmon on 1/13/2021.
//

#include "GButton.h"

const int BUTTON_WIDTH = 300;
const int BUTTON_HEIGHT = 200;

GButton::GButton(){
    mPosition.x = 0;
    mPosition.y = 0;

    mCurrentSprite = GButtonSprite::BUTTON_SPRITE_MOUSE_OUT;
}

void GButton::setPosition( int x, int y ){
    mPosition.x = x;
    mPosition.y = y;
}

void GButton::setTexture(GTexture texture){
    this->mTexture = texture;
}

void GButton::setSpriteClips(SDL_Rect *spriteClips){
    this->mSpriteClips = spriteClips;
}

void GButton::handleEvent( SDL_Event* e ){
    //If mouse event happened
    if( e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP ){
        //Get mouse position
        int x, y;
        SDL_GetMouseState( &x, &y );

        //Check if mouse is in button
        bool inside = true;

        //Mouse is left of the button
        if( x < mPosition.x ){
            inside = false;
        }
            //Mouse is right of the button
        else if( x > mPosition.x + BUTTON_WIDTH ){
            inside = false;
        }
            //Mouse above the button
        else if( y < mPosition.y ){
            inside = false;
        }
            //Mouse below the button
        else if( y > mPosition.y + BUTTON_HEIGHT ){
            inside = false;
        }
        //Mouse is outside button
        if( !inside ){
            mCurrentSprite = GButtonSprite::BUTTON_SPRITE_MOUSE_OUT;
        }
            //Mouse is inside button
        else{
            //Set mouse over sprite
            switch( e->type ){
                case SDL_MOUSEMOTION:
                    mCurrentSprite = GButtonSprite::BUTTON_SPRITE_MOUSE_OVER_MOTION;
                    break;

                case SDL_MOUSEBUTTONDOWN:
                    mCurrentSprite = GButtonSprite::BUTTON_SPRITE_MOUSE_DOWN;
                    break;

                case SDL_MOUSEBUTTONUP:
                    mCurrentSprite = GButtonSprite::BUTTON_SPRITE_MOUSE_UP;
                    break;
            }
        }
    }
}

void GButton::render( ){
    //Show current button sprite
    mTexture.render( mPosition.x, mPosition.y, &mSpriteClips[ mCurrentSprite ] );
}