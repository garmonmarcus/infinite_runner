//
// Created by Marcus Garmon on 1/13/2021.
//

#ifndef ENDLESSRUNNERPOC_GBUTTON_H
#define ENDLESSRUNNERPOC_GBUTTON_H
#include <SDL2/SDL.h>
#include "component/GTexture.h"

class GButton {
public:
    //Initializes internal variables
    GButton();

    //Sets top left position
    void setPosition( int x, int y );

    //Handles mouse event
    void handleEvent( SDL_Event* e );

    //Shows button sprite
    void render( );

    void setTexture(GTexture texture);

    void setSpriteClips(SDL_Rect* spriteClips);
private:
    enum GButtonSprite
    {
        BUTTON_SPRITE_MOUSE_OUT = 0,
        BUTTON_SPRITE_MOUSE_OVER_MOTION = 1,
        BUTTON_SPRITE_MOUSE_DOWN = 2,
        BUTTON_SPRITE_MOUSE_UP = 3,
        BUTTON_SPRITE_TOTAL = 4
    };

    GTexture mTexture;
    SDL_Rect* mSpriteClips{};

    //Top left position
    SDL_Point mPosition{};

    //Currently used global sprite
    GButtonSprite mCurrentSprite;
};


#endif //ENDLESSRUNNERPOC_GBUTTON_H
