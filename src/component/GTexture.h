//
// Created by Marcus Garmon on 1/13/2021.
//

#ifndef ENDLESSRUNNERPOC_GTEXTURE_H
#define ENDLESSRUNNERPOC_GTEXTURE_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>


class GTexture {
public:
    //Initializes variables
    GTexture();

    //Deallocates memory
    ~GTexture();

    //Loads image at specified path
    bool loadFromFile( std::string path );

    //Creates image from font string
    bool loadFromRenderedText( std::string textureText, SDL_Color textColor );

    //Deallocates texture
    void free();

    void setRenderer(SDL_Renderer* renderer);

    void setFont(TTF_Font* font);

    //Set color modulation
    void setColor( Uint8 red, Uint8 green, Uint8 blue );

    //Set blending
    void setBlendMode( SDL_BlendMode blending );

    //Set alpha modulation
    void setAlpha( Uint8 alpha );

    //Renders texture at given point
    void render( int x, int y, SDL_Rect* clip = nullptr, double angle = 0.0, SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE );

    //Gets image dimensions
    int getWidth();
    int getHeight();


private:
    //The actual hardware texture
    SDL_Texture* mTexture;
    SDL_Renderer* mRenderer;
    TTF_Font* mFont;

    //Image dimensions
    int mWidth;
    int mHeight;
};


#endif //ENDLESSRUNNERPOC_GTEXTURE_H
