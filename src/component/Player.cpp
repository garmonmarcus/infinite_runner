//
// Created by Marcus Garmon on 1/13/2021.
//

#include "Player.h"
#include "utils/game_utils.h"
#include "component/GEvent.h"

Player::Player()= default;

Player::~Player() = default;

void Player::experienceGained(long gainedExperience) {
    this->mExperience = this->mExperience + gainedExperience;
    bool leveled = game_utils::checkIfLeveled(this->mExperience);

    if(leveled){
        this->mAvailableAttrPts++;
//        GEvent lEvent;
//        game_utils::eventTriggered(lEvent);
    }
}

void Player::setTexture(const GTexture& playerTexture){
    this->mTexture = playerTexture;
}

int Player::getAvailableAttributes() const {
    return mAvailableAttrPts;
}

void Player::increaseAttribute(PlayerAttributeName attributeName) {
//    if(this->mAvailableAttrPts > 0){
//        ++this->mAttributes[attributeName];
//    } else {
//        // No points available
//    }
}

void Player::setAnimation(Animation animation) {
    this->mAnimation = animation;
}

void Player::update(){

}

void Player::handleEvent(){

}


