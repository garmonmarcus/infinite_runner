//
// Created by Marcus Garmon on 1/13/2021.
//

#ifndef ENDLESSRUNNERPOC_PLAYER_H
#define ENDLESSRUNNERPOC_PLAYER_H


#include <map>
#include <string>
#include "GTexture.h"
#include "component/Animation.h"

enum PlayerAttributeName {
    SPEED, JUMP_DISTANCE,
};

class Player {
public:
    Player();
    ~Player();

    int getAvailableAttributes() const;

    void experienceGained(long gainedExperience);

    void setTexture(const GTexture& playerTexture);

    void increaseAttribute(PlayerAttributeName attributeName);

    void handleEvent();

    void setAnimation(Animation animation);

    void update();

    float speed;

private:
    int mAvailableAttrPts{};

    long mExperience{};

    std::map<PlayerAttributeName, int, std::hash<int>> mAttributes;

    GTexture mTexture;

    Animation mAnimation;

};


#endif //ENDLESSRUNNERPOC_PLAYER_H
