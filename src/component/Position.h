//
// Created by Marcus Garmon on 1/14/2021.
//

#ifndef ENDLESSRUNNERPOC_POSITION_H
#define ENDLESSRUNNERPOC_POSITION_H

#include <utils/vector2d.h>

class Position {
public:
    Position();
    Position(float x, float y);

    float x;
    float y;

private:
   //TODO add vector2d as the object that stores position
};

#endif //ENDLESSRUNNERPOC_POSITION_H
