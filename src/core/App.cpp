//
// Created by Marcus Garmon on 1/14/2021.
//

#include "App.h"

#include "Game.h"

const int SCREEN_WIDTH = 840;
const int SCREEN_HEIGHT = 600;

namespace {
}

Application::Application() {
//    SDL_CHECK(SDL_Init(SDL_INIT_VIDEO));
}

Application::~Application() {
    close();
}

void Application::run() {

    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ){
        printf( "Warning: Linear texture filtering not enabled!" );
    }

    window = SDL_CreateWindow(
            "Endless Runner Pilot v0.01",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN );

    if( window == nullptr ){
        printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
        SDL_Log("Window could not be created! SDL Error: %s\n", SDL_GetError());
        isRunning = false;
    }
    else{
        //Create renderer for window
        renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
        if( renderer == nullptr )
        {
            printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
            isRunning = false;
        }
        else {
            // Initialize PNG loading
            if( IMG_Init( IMG_INIT_PNG ) != IMG_INIT_PNG  ){
                SDL_Log("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
                isRunning = false;
            }
            //Initialize SDL_ttf
            if( TTF_Init() == -1 )
            {
                printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
                isRunning = false;
            }
        }

        //Get window surface
        screenSurface = SDL_GetWindowSurface( window );
        isRunning = true;
    }


    Game game(renderer);

    game.init();

    double time         = 0.0;
    double accumulator  = 0.0;
    double current_time = SDL_GetTicks();
    double new_time     = 0.0;
    double frame_time   = 0.0;

    while(isRunning && game.state != Game::State::QUIT){

        // 60 updates per second. We divide 1000 by 60 instead of 1 because sdl operates on milliseconds
        // not nanoseconds.
        const constexpr double dt = 1000.0 / 60.0;

        new_time     = SDL_GetTicks();
        frame_time   = new_time - current_time;
        current_time = new_time;

        accumulator += frame_time;

        game.events();

        while (accumulator >= dt){
            game.update(accumulator);
            accumulator -= dt;
            time += dt;
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear( renderer );

        game.render();

        SDL_RenderPresent( renderer );
    }

    close();
}

void Application::close(){

    //Destroy window
    SDL_DestroyRenderer( renderer );
    SDL_DestroyWindow( window );
    window = nullptr;
    renderer = nullptr;

    //Quit SDL subsystems
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}