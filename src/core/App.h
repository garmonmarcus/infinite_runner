//
// Created by Marcus Garmon on 1/14/2021.
//

#ifndef ENDLESSRUNNERPOC_APP_H
#define ENDLESSRUNNERPOC_APP_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>


class Application {
public:
    Application();
    ~Application();

    void run();
    void close();

    bool isRunning = false;


private:
    SDL_Renderer* renderer;
    SDL_Window* window = nullptr;
    SDL_Surface* screenSurface = nullptr;
};


#endif //ENDLESSRUNNERPOC_APP_H
