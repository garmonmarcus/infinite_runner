//
// Created by Marcus Garmon on 1/14/2021.
//

#include "Factories.h"

#include "component/Player.h"
#include "component/Position.h"
#include <entt/entity/registry.hpp>

entt::entity makePlayer(entt::registry &reg, SDL_Renderer* renderer) {
    const entt::entity playerEntity = reg.create();
    auto &player = reg.emplace<Player>(playerEntity);

    /*
     * TODO List
     * Load player last position from saved location
     * Load player speed from last location
     * Load all of player attributes from saved location
     */

    player.speed = 5;
    float savedPositionX = 0.0f;
    float savedPositionY = 0.0f;
    auto &position = reg.emplace<Position>(playerEntity);
    position.x = savedPositionX;
    position.y = savedPositionY;

    return playerEntity;
}

entt::entity makeGround(entt::registry &reg, SDL_Renderer* renderer, float x, float y){
    const entt::entity ground = reg.create();
    auto &position = reg.emplace<Position>(ground);
    position.x = x;
    position.y = y;

    return ground;
}
