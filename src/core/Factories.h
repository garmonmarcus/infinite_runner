//
// Created by Marcus Garmon on 1/14/2021.
//

#ifndef ENDLESSRUNNERPOC_FACTORIES_H
#define ENDLESSRUNNERPOC_FACTORIES_H

#include <entt/entity/fwd.hpp>

#include <SDL2/SDL.h>

entt::entity makePlayer(entt::registry &, SDL_Renderer* renderer);

entt::entity makeGround(entt::registry&, SDL_Renderer* renderer, float x, float y);
#endif //ENDLESSRUNNERPOC_FACTORIES_H
