//
// Created by Marcus Garmon on 1/13/2021.
//

#include "Game.h"
#include "Factories.h"

const int SCREEN_WIDTH = 840;
const int SCREEN_HEIGHT = 600;

Game::Game(SDL_Renderer* renderer){
    this->mRenderer=renderer;
}

void Game::init(){
    const entt::entity player = makePlayer(mRegistry, mRenderer);

    //TODO Draw the ground
    const entt::entity ground = makeGround(mRegistry, mRenderer, 0, 0);
}

void Game::initLoadScreen() {

}

void Game::startGame() {

}

void Game::events() {
    // Process all user and system events.
    while (SDL_PollEvent(&mEvent) != 0){
        switch (mEvent.type){
            case SDL_QUIT:
                state = State::QUIT;
                break;
        }
    }
}

void Game::update(const double time) {

    mRunnerSystem.update(time, mRegistry);
    mExpGenerator.update(time, mRegistry);

    if (state == State::playing) {
//        mCollisionSystem.update(time, mRegistry);

    }
    else if(state == State::paused){

    }
    else if(state == State::settings){

    }
}

void Game::render() {

    // Grab ref to the renderer and the entt registry
    if(state == State::playing){
        mRenderSystem.renderGrid(mRenderer, SCREEN_WIDTH, SCREEN_HEIGHT);
        mRenderSystem.render(mRenderer, mRegistry);
    }
    else if(state == State::paused){
        // Render pause UI
    }
    else if(state == State::settings){
        // Render settings UI
    }
}