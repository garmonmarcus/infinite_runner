//
// Created by Marcus Garmon on 1/13/2021.
//

#ifndef ENDLESSRUNNERPOC_GAME_H
#define ENDLESSRUNNERPOC_GAME_H
#include <iostream>

#include <cstdio>
#include <string>
#include <SDL2/SDL.h>
#include <entt/entt.hpp>
#include <system/RenderSystem.h>
#include <system/RunnerSystem.h>
#include <system/ExpGenerator.h>
#include <system/CollisionSystem.h>

class Game {
public:
    enum class State {
        playing,
        paused,
        settings,
        QUIT
    };

    State state = State::playing;

    Game(SDL_Renderer* renderer);

    void init();
    void initLoadScreen();
    void startGame();

    void update(double time);

    void events();

    void render();

private:
    entt::registry mRegistry;
    entt::dispatcher mDispatcher;

    RenderSystem mRenderSystem;
    RunnerSystem mRunnerSystem;
    CollisionSystem mCollisionSystem;
    ExpGenerator mExpGenerator;

    SDL_Renderer* mRenderer;
    SDL_Event mEvent;
};

#endif //ENDLESSRUNNERPOC_GAME_H
