//
// Created by Marcus Garmon on 1/16/2021.
//

#ifndef ENDLESSRUNNERPOC_COLLISIONSYSTEM_H
#define ENDLESSRUNNERPOC_COLLISIONSYSTEM_H

#include <entt/fwd.hpp>

class CollisionSystem {
public:
    void update(double time, entt::registry& registry);
};


#endif //ENDLESSRUNNERPOC_COLLISIONSYSTEM_H
