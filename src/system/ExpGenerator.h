//
// Created by Marcus Garmon on 1/14/2021.
//

#ifndef ENDLESSRUNNERPOC_EXPGENERATOR_H
#define ENDLESSRUNNERPOC_EXPGENERATOR_H

#include <entt/fwd.hpp>

struct ExpGenerator {
    void update(double time, entt::registry& registry);
};


#endif //ENDLESSRUNNERPOC_EXPGENERATOR_H
