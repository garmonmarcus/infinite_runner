//
// Created by Marcus Garmon on 1/14/2021.
//

#include "RenderSystem.h"

#include <entt/entity/registry.hpp>
#include <utils/vector2d.h>
#include "component/Player.h"
#include "component/Position.h"

void RenderSystem::render(SDL_Renderer *renderer, entt::registry &registry) {

    auto view = registry.view<Position>();

}

void RenderSystem::renderGrid(SDL_Renderer *renderer, int screenHeight, int screenWidth){

    // First we set the rectangle fill colour to that of the spritecomponents.
    SDL_SetRenderDrawColor(renderer, 0.0f, 255.0f, 0.0f, 0.0f);

    // Draw Horizontal Lines
    for(int y = 0; y < 1200; y+=40){
        SDL_RenderDrawLine(renderer, 0, y+40, 1200, y+40);
    }

    // Draw Vertical Lines
    for(int x = 0; x < 1200; x+=40){
        SDL_RenderDrawLine(renderer, x+40, 0, x+40, 1200);
    }
}

