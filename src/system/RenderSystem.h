//
// Created by Marcus Garmon on 1/14/2021.
//

#ifndef ENDLESSRUNNERPOC_RENDERSYSTEM_H
#define ENDLESSRUNNERPOC_RENDERSYSTEM_H
#include <entt/entity/fwd.hpp>
#include <SDL2/SDL.h>

class RenderSystem {
public:
    void render(SDL_Renderer* renderer,  entt::registry& registry);
    void renderGrid(SDL_Renderer* renderer, int screenWidth, int screenHeight);
};

#endif //ENDLESSRUNNERPOC_RENDERSYSTEM_H
