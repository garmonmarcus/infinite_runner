//
// Created by Marcus Garmon on 1/16/2021.
//

#include "RunnerSystem.h"
#include <entt/entt.hpp>
#include "component/Player.h"
#include "component/Position.h"

void RunnerSystem::update(const double time, entt::registry& registry) {

    // We only need to update the player position, since the ai wil be managed by the ai system.
    auto player_view = registry.view<Player, Position>();
    player_view.each([&](auto& plr, auto& pos) {
        // Need to update the playerPosition
        pos.setX(pos.getX() + (plr.speed));
    });

//    for (const entt::entity e : player_view) {
//        Position pos = player_view.get<Position>(e);
//        float x = pos.getX();
//        SDL_log(pos.getX());
//    }
}
