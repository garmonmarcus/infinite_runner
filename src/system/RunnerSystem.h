//
// Created by Marcus Garmon on 1/16/2021.
//

#ifndef ENDLESSRUNNERPOC_RUNNERSYSTEM_H
#define ENDLESSRUNNERPOC_RUNNERSYSTEM_H

#include <entt/fwd.hpp>

class RunnerSystem {
public:
    void update(double time, entt::registry& registry);
};


#endif //ENDLESSRUNNERPOC_RUNNERSYSTEM_H
