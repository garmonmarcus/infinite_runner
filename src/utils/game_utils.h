//
// Created by Marcus Garmon on 1/13/2021.
//

#ifndef ENDLESSRUNNERPOC_GAME_UTILS_H
#define ENDLESSRUNNERPOC_GAME_UTILS_H


#include "component/GEvent.h"

class game_utils {
public:
    static bool checkIfLeveled(long exp);
    static void eventTriggered(GEvent event);
};


#endif //ENDLESSRUNNERPOC_GAME_UTILS_H
