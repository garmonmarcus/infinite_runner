//
// Created by Marcus Garmon on 1/14/2021.
//

#ifndef ENDLESSRUNNERPOC_VECTOR2D_H
#define ENDLESSRUNNERPOC_VECTOR2D_H


class Vector2d {
public:
    Vector2d(float x, float y);

    Vector2d add(float x, float y);
    Vector2d add(Vector2d vector2D);
    Vector2d subtract(float x, float y);
    Vector2d subtract(Vector2d vector2D);

    float x;
    float y;

private:

};


#endif //ENDLESSRUNNERPOC_VECTOR2D_H
